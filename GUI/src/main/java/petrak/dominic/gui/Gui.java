package petrak.dominic.gui;

import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Diese Klasse beinhaltet die Gui
 *
 * @author Dominic
 */
public class Gui {

    JPanel panel; 
    
    JLabel functionLabel;
    JLabel derivationLabel;
    JLabel startValueLabel;
    JLabel precisionLabel;

    JTextField functionTextfield;
    JTextField derivationTextfield;
    JTextField startValueTextfield;
    JTextField precisionTextfield;

    JTextArea output;

    JButton calculate;

    public Gui() {
        
        panel = new JPanel();

        this.functionLabel = new JLabel("Funktion");
        this.derivationLabel = new JLabel("Ableitung");
        this.startValueLabel = new JLabel("Startwert");
        this.precisionLabel = new JLabel("Genauigkeit");

        this.functionTextfield = new JTextField();
        this.derivationTextfield = new JTextField();
        this.startValueTextfield = new JTextField();
        this.precisionTextfield = new JTextField();

        this.functionTextfield.setSize(100, 10);
        this.derivationTextfield.setSize(100, 10);
        this.startValueTextfield.setSize(100, 10);
        this.precisionTextfield.setSize(100, 10);

        this.calculate = new JButton("Berechne");
        this.output = new JTextArea();

        GroupLayout layout = new GroupLayout(panel);
        
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(functionLabel)
                        .addComponent(functionTextfield))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(derivationLabel)
                        .addComponent(derivationTextfield))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(startValueLabel)
                        .addComponent(startValueTextfield))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(precisionLabel)
                        .addComponent(precisionTextfield))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(calculate)
                        .addComponent(output))
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                        .addComponent(functionLabel)
                        .addComponent(functionTextfield))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                        .addComponent(derivationLabel)
                        .addComponent(derivationTextfield))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                        .addComponent(startValueLabel)
                        .addComponent(startValueTextfield))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                        .addComponent(precisionLabel)
                        .addComponent(precisionTextfield))
                .addComponent(calculate)
                .addComponent(output)
        );
        
        panel.setLayout(layout);

        panel.add(functionLabel);
        panel.add(functionTextfield);
        panel.add(derivationLabel);
        panel.add(derivationTextfield);
        panel.add(startValueLabel);
        panel.add(startValueTextfield);
        panel.add(calculate);
        panel.add(output);

    }
    
    public String getFunktionText() {
        return functionTextfield.getText();
    }

    public String getAbleitungText() {
        return derivationTextfield.getText();
    }

    public String getStartwertText() {
        return startValueTextfield.getText();
    }
    
    public String getGenauigkeitText() {
        return precisionTextfield.getText();
    }
    
    public void addToTextArea(String string) {
        output.append(string);
        panel.revalidate();
    }
    
    public JPanel getPanel() {
        return panel;
    }
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Newtonverfahren");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(500, 500));
        
        Gui gui = new Gui();
        
        frame.add(gui.getPanel());

        frame.pack();
        frame.setVisible(true);
    }
}

