/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petrak.dominic.newtonverfahren.guiLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import petrak.dominic.newtonverfahren.entities.Function;
import petrak.dominic.newtonverfahren.logic.Newton;
import petrak.dominic.newtonverfahren.main.Window;
import petrak.dominic.newtonverfahren.panels.Panel;

/**
 *
 * @author Dominic
 */
public class Calculate implements ActionListener {
    
    Window frame;
    
    public Calculate(Window frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Panel p = frame.getPanel();
            Newton n = new Newton();
            p.fillTable(n.calculate(new Function(frame.getPanel().getFunctionValue()),
                    Double.parseDouble(frame.getPanel().getRangeSmallest()),
                    Double.parseDouble(frame.getPanel().getRangeHeighest())));
            //frame.revalidate();
        } catch (Exception ex) {
            Logger.getLogger(Window.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

