package petrak.dominic.newtonverfahren.main;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import petrak.dominic.newtonverfahren.guiLogic.Calculate;
import petrak.dominic.newtonverfahren.panels.Panel;

/**
 * Diese Klasse bildet das Fenster ab.
 *
 * @author Dominic
 */
public class Window extends JFrame {

    private Panel panel;

    public Window() {
        super("Newton-Verfahren");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
                        
        JButton calculate = new JButton("Berechne");
        calculate.addActionListener(new Calculate(this));
        panel = new Panel(calculate);
        
        add(panel);

        pack();
        setVisible(true);
    }
    
    public Panel getPanel() {
        return panel;
    }

}
