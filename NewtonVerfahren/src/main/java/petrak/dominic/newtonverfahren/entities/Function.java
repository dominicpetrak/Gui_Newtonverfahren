/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petrak.dominic.newtonverfahren.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import javax.script.ScriptException;

/**
 *
 * @author Dominic
 */
public class Function {
    
    private final String POLYNOMS_WITH_DELIMITER = "(?=\\+)|(?=\\-)";
    
    private List<String> polynoms;
    private String function;
    private String derivation;
    
    public Function(String expression) {                
        this.function = expression;
        this.polynoms = splitExpression(expression);        
        this.derivation = derivate();
    }
    
    /**
     * In dieser Methode werden die Polynome verarbeitet. Es wird in Abhängigkeit
     * der Syntax (enthält ^, x, gar nichts) verarbeitet.
     */
    private String derivate() {  
        StringBuilder builder = new StringBuilder();
        
        polynoms.forEach(s -> {
            if(s.contains("^")) {
                builder.append(calculateDerivation(s.split("[\\^]")));
            }
            else if(s.contains("x") && !s.contains("^")) {
                builder.append(s.replace("x", ""));
            }            
        });      
        if (builder.toString().startsWith("+")) {
            return builder.deleteCharAt(0).toString();
        }
        return builder.toString();
    }
    
    /**
     * Diese Methode splittet den gesamten Ausdruck in seine einzelnen Polynome und
     * fügt vor diesen ein entsprechendes Vorzeichen an. Die Polynome werden als Liste
     * zurückgegeben.
     * @param exp
     * @return 
     */
    private List<String> splitExpression(String exp) {       
        return new ArrayList<String>() {
            {
                Arrays.asList(exp.split(POLYNOMS_WITH_DELIMITER)).forEach(s -> {
                    //Hier wird geprueft, ob die Polynome ein negatives und ueberhaupt ein Vorzeichen besitzen.
                    //Ist das nicht der Fall, sind sie implizit positiv.
                    add(!s.startsWith("+") && !s.startsWith("-") ? "+" + s : s);
                });
            }
        };
    }
    
    /**
     * Diese Methode berechnet die Ableitung für ein Polynom mit angegebenen Grad.
     * @param array
     * @return 
     */
    private String calculateDerivation(String[] array) {
        
        Pattern pattern = Pattern.compile("[^0-9]");        
        StringBuilder builder = new StringBuilder();
        double exp = pattern.matcher(array[0]).matches() ? Double.parseDouble(array[0].replaceAll("[xX]", "")) : 1.0;
        double grade = Double.parseDouble(array[1]);        
        int reducedGrade = (int) grade-1;
        
        builder.append(exp * grade);
        
        if(reducedGrade > 1) {
            builder.append("x^");
            builder.append(reducedGrade);
        }
        else if(reducedGrade == 1) {
            builder.append("x");
        }
        
        if(!builder.toString().startsWith("-")) {
            builder.insert(0, "+");
        }
        return builder.toString();
    }
    
    /**
     * Diese Methode berechnet eine Potenz
     * @param array 
     */
    private Double calculatePotency(String[] array) {
        String[] pendingMultis = null;
        double exp;
        double potency;
        double result;
        if(array[0].contains("*")) {
            pendingMultis = array[0].split("[\\*]");
            array[0] = array[0].substring(array[0].lastIndexOf("*")+1);
        }
        
        exp = Double.parseDouble(array[0]);
        potency = Double.parseDouble(array[1]);
        result = exp;
                        
        for(int i = 0; i <= potency; i++) {
            result = result * exp;
        }    
        if(pendingMultis != null) {
            String[] values = new String[pendingMultis.length];
            for(int i = 0; i < pendingMultis.length-1; i++) {
                values[i] = pendingMultis[i];
            }
            values[pendingMultis.length-1] = String.valueOf(result);
            result = calculateMultiplication(values);
        }
        return result;
    }
    
    private Double calculateMultiplication(String[] array) {
        // Hier knallt's, weil ein Plus an Stelle 0 enthalten ist.
        double tempResult = Double.parseDouble(array[0]);
        for(int i = 1; i < array.length; i++) {
            tempResult = tempResult * Double.parseDouble(array[i]);
        }
        return tempResult;
    }
    
    /**
     * Diese Methode berechnet einen übergebenen Ausdruck. Es ist notwendig,
     * dass zu diesem Zeitpunkt bereits alle x ersetzt wurden.
     * @param exp
     * @return
     * @throws ScriptException 
     */
    private Double eval(String exp) throws ScriptException {
        double tempResult = 0.0;
        for (String s : splitExpression(exp)) {
            if (s.contains("^")) {
                tempResult = tempResult + calculatePotency(s.split("[\\^]"));
            }
            else if(s.contains("*")){
                tempResult = tempResult + calculateMultiplication(s.split("[\\*]"));
            }
            else {
                tempResult = tempResult + Double.parseDouble(s);
            }
        }     
        return tempResult;
    }

    public Double getDerivationValue(double x) throws Exception {        
        double eval;
        if((eval = eval(derivation.replaceAll("x","*" + x))) > 0) {
            return eval;
        }
        else {
            throw new Exception("Derivation 0!");
        }        
    }
    
    public Double getFunctionValue(double x) throws ScriptException {        
        return eval(function.replaceAll("x", "*" + x));
    }
    
    
    public String getOriginalExpression() {
        return function;
    }
    
    public String getOriginaleDerivation() {
        return derivation;
    }
    
    
}
