package petrak.dominic.newtonverfahren.panels;

import java.util.Iterator;
import java.util.Map;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * Diese Klasse bildet die Eingabe der Funktion und der Startparameter ab.
 * @author Dominic
 */
public class Panel extends JPanel {
    
    JLabel functionLabel;
    JLabel rangeSmallestLabel; 
    JLabel rangeHeighestLabel; 

    JTextField functionTextfield; 
    JTextField rangeSmallestTextfield; 
    JTextField rangeHeighestTextfield; 
    
    JTable table;
    
    JButton calculate;
    
    public Panel(JButton calculate) {             
        
        this.functionLabel = new JLabel("Bitte geben Sie hier Ihre Funktion ein: ");
        this.rangeSmallestLabel = new JLabel("Bitte geben Sie hier den kleinsten Wert des Prüfbereichs ein: ");
        this.rangeHeighestLabel = new JLabel("Bitte geben Sie hier den größten Wert des Prüfbereichs ein: ");
        
        this.functionTextfield = new JTextField();
        this.rangeSmallestTextfield = new JTextField(2);
        this.rangeHeighestTextfield = new JTextField(2);
        
        this.functionTextfield.setSize(100, 10);
        this.rangeSmallestTextfield.setSize(100, 10);
        this.rangeHeighestTextfield.setSize(100, 10);
        
        this.table = new JTable(10, 2);
        
        GroupLayout layout = new GroupLayout(this);
        
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(functionLabel)
                                .addComponent(functionTextfield))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(rangeSmallestLabel)
                                .addComponent(rangeSmallestTextfield))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(rangeHeighestLabel)
                                .addComponent(rangeHeighestTextfield))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(calculate)
                                .addComponent(table))
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                                .addComponent(functionLabel)
                                .addComponent(functionTextfield))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                                .addComponent(rangeSmallestLabel)
                                .addComponent(rangeSmallestTextfield))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER, false)
                                .addComponent(rangeHeighestLabel)
                                .addComponent(rangeHeighestTextfield))
                        .addComponent(calculate)
                        .addComponent(table)
        );
        
        setLayout(layout);
         
        add(functionLabel);
        add(functionTextfield);
        add(rangeSmallestLabel);
        add(rangeSmallestTextfield);
        add(rangeHeighestLabel);
        add(rangeHeighestTextfield);
        add(calculate);    
        add(table);
        
    }
    
    public void fillTable(Map<Double, Double> map) {
        Object[][] values = new Object[map.size()][2];
        Iterator it = map.entrySet().iterator();

        int i = 0;
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            values[i][0] = entry.getKey();
            values[i][1] = entry.getValue();
            i++;
        }
        table = new JTable(values, new String[]{"X-Wert", "Funktionswert"});
        this.revalidate();
    }
    
    public String getFunctionValue() {
        return functionTextfield.getText();
    }
    
    public String getRangeSmallest() {
        return rangeSmallestTextfield.getText();
    }
    
    public String getRangeHeighest() {
        return rangeHeighestTextfield.getText();
    }
    
}
