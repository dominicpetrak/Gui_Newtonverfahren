package petrak.dominic.newtonverfahren.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.script.ScriptException;
import petrak.dominic.newtonverfahren.entities.Function;

/**
 *
 * @author Dominic
 */
public class Newton {
    
    private final int REPEATS = 10;

    public Newton() {}
    
    /*
    Ist die Berechnung von xn fehlerhaft?
    */
    public Map<Double, Double> calculate(Function function, double rangeSmallest, double rangeHeighest) throws ScriptException, Exception {        
        double value;   
        Map<Double, Double> results = new TreeMap<>();
        for (double next : range(rangeSmallest, rangeHeighest)) {
            value = function.getFunctionValue(next) / function.getDerivationValue(next);
            results.put(next, value);
        }     
        return results;
    }
    
    private List<Double> range(double start, double end) {
        List<Double> toReturn = new ArrayList<>();
        double step = (start + end) / 10;

        while (end >= start) {
            toReturn.add(end);
            end = (double) Math.round((end - step) * 100) / 100;
        }
        return toReturn;
    }
    
}
